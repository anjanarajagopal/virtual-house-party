{ sources ? import ./sources.nix }:

let
    pkgs =
        import sources.nixpkgs { overlays = [ (import sources.nixpkgs-mozilla) ]; };
    channel = "1.44.1";
    targets = [ ];
    chan = pkgs.rustChannelOfTargets channel null targets;
in chan