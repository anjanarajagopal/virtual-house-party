let
    sources = import ./nix/sources.nix;
    rust = import ./nix/rust.nix { inherit sources; };
    pkgs = import sources.nixpkgs {};

    inherit (pkgs.lib) optional optionals;

    erlang = pkgs.beam.interpreters.erlangR22;
    elixir = pkgs.beam.packages.erlangR22.elixir_1_10;
    nodejs = pkgs.nodejs-12_x;
    gst = pkgs.gst_all_1;
    stdenv = pkgs.stdenv;
in
pkgs.mkShell {
    buildInputs = [
        # core programming languages and tools
        elixir
        nodejs
        pkgs.yarn
        rust

        # Certificates
        pkgs.cacert

        # libraries
        gst.gstreamer
        gst.gst-plugins-base
        gst.gst-plugins-good
        gst.gst-plugins-bad
        gst.gst-plugins-ugly

        # development tooling
        pkgs.git
        pkgs.ripgrep
    ]
    ++ optional stdenv.isLinux pkgs.inotify-tools
    ++ optionals stdenv.isDarwin (with pkgs.darwin.apple_sdk.frameworks; [
        CoreFoundation
        CoreServices
    ]);
}